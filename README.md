# producer

## Info

Liest Smartphone Accounts mit verschlüsseltem Passwort (rot13) aus einer CSV und sendet sie an einen Broker.

## Start des Containers

```
podman run -d -e KSERVER=192.168.86.42 -e KPORT=9092  --name producer registry.gitlab.com/claus.boehmer/producer
```